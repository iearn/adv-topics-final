# Make sure to update this README file so that it describes your project #

### What is the project about ###

This project...

### Getting Set Up ###

This project use NodeJS and Gulp. In order to get started you will have to do these things after cloning the reposiroty

* Open the terminal and cd to the project folder
* run this command to install the required NodeJS modules(the dependencies are in package.json): npm install

### Building the App ###

To compile the app for distribution:

* Open the terminal and cd to the project folder
* Enter this command to run the Gulp tasks: gulp
* The compiled code for distribution will be in the 'dist' folder