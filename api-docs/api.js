YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "BikeDetails",
        "BikeList",
        "SignUpModule"
    ],
    "modules": [
        "rpgbacon"
    ],
    "allModules": [
        {
            "displayName": "rpgbacon",
            "name": "rpgbacon"
        }
    ],
    "elements": []
} };
});