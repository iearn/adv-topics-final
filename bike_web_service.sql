-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2017 at 03:58 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bike_web_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `bikes`
--

CREATE TABLE `bikes` (
  `id` int(11) NOT NULL,
  `company` varchar(32) NOT NULL,
  `year` varchar(32) NOT NULL,
  `color` varchar(32) NOT NULL,
  `active` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikes`
--

INSERT INTO `bikes` (`id`, `company`, `year`, `color`, `active`) VALUES
(1, 'strider', '2015', 'yellow', 'no'),
(2, 'Schwinn', '1998', 'red', 'no'),
(3, 'Marvel', '2017', 'red', 'no'),
(4, 'Huff', '1999', 'orange', 'no'),
(5, 'Huff', '1999', 'orange', 'no'),
(6, 'Honda', '1999', 'red', 'no'),
(7, 'Marvel', '2017', 'red', 'no'),
(8, 'Foo ', 'Bar', 'GRRRRRRRRRRRREEEEEEEEEn', 'no'),
(9, 'French', '2015', 'yellow', 'yes'),
(10, 'Taco Johns', '1998', 'Blue', 'no'),
(11, 'Taco Johns', '1998', 'Blue', 'no'),
(12, 's', 's', 's', 'yes'),
(13, 'dd', 'dd', 'dd', 'no'),
(14, 'French CO', '19998', 'ORANGE', 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bikes`
--
ALTER TABLE `bikes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bikes`
--
ALTER TABLE `bikes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
