window.addEventListener("load", function(){


	// Set up all the modules here...

	// var signupModule = myNameSpace.SignUpModule("signup-container", function(data){
	// 	alert("This is the callback!\nTODO: send this data to the server for saving!")
	// 	console.log(data);
	// });

	var BikeList;
	var BikeDetails;
	var btnAdd = document.getElementById("addBike");

	// btnAdd.addEventListener("click", function(){
	// 	var b = {id : 0, company : "", year : "", color: "", active: ""};
	// 	BikeDetails.setBook(b);
	// });

	BikeDetails = rpgbacon.BikeDetails.init({

		target: document.getElementById("BikeDetails"),
		
		saveCallBack: function(bike){	
			console.log(bike);		
			if(bike.id > 0){
				da.putBike(bike, function(response){
					bikeList.refresh();
				});

			}else{
				da.postBike(bike, function(response){
					var bike = JSON.parse(response);

					console.log(bike);

					bikeList.addBike(bike);
				});
		
			}

		}
	});


	da = rpgbacon.BikeDataAccess();

	var bikes = da.getAllBikes(function(response){
			var bikes = JSON.parse(response);

			bikeList = rpgbacon.BikeList.init({
				target:document.getElementById("BikeList"), 
				data: bikes,
				selectedCallBack: function(bike){
					BikeDetails.setBike(bike);
				},
				deleteBikeCallBack: function(bike){
					// console.log(bike.id);

					da.deleteBike(bike.id, bike, function(response){
						// company.log(JSON.parse(response));
					});
					bikeList.removeBike(bike.id);
				}
			});
	});
	// console.log(bikes);

// Test code for all the function on the BikeDataAccess
	// console.log(rpgbacon.ajax);

	// function handleResponse(response){
	// 	alert("Here's your response:\n" + response);
	// }

	// da = rpgbacon.BikeDataAccess();

	// da.getAllBikes(handleResponse);

	// da.getBikeById(3, handleResponse);


	// var obj = {
	// 	id: '0',
	// 	company : 'Marvel',
	// 	year : '2017',
	// 	color : 'red',
	// 	active : 'yes' 
	// }

	// console.log(JSON.stringify(obj));

	// // da.postBike(obj, handleResponse);

	
	// obj['id'] = '0';
	// obj['company'] = 'Marvel';
	// obj['year'] = '2018';
	// obj['color'] = 'red';
	// obj['active'] = 'yes'; 
	// da.putBike(obj, handleResponse);

	// var obj2 = {
	// 	id: '3',
	// 	company : 'Marvel',
	// 	year : '2017',
	// 	color : 'red',
	// 	active : 'yes' 
	// }
	// da.deleteBike(3, obj2, handleResponse);

	// da.getAllBikes(handleResponse);
});