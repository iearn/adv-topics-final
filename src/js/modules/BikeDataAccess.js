var rpgbacon = rpgbacon || {};

rpgbacon.BikeDataAccess = function(){

	var urlWebService;

	if(location.host == "localhost"){
	    urlWebService = "http://localhost/adv-topics-final/web-service/bikes/";
	}else{
	    urlWebService = "http://rpgbacon.com/web-service/web-service/bikes/";
	}

	// var BikeId = 2;
	//getBikeById(BikeId, handleResponse);

		
	// var newBike = {"title":"Bob's Life Story", "author":"Bob"}; 
	//Note: no need add an 'id' the newBike object, the database will assign the id
	//and the newBike, including the id, will be in the response
	// postBike(newBike, handleResponse);


	// var updatedBike = {"id":1, "title":"Foo bar", "author":"Bob"};
	// putBike(updatedBike, handleResponse);


	/*
	YOU JOB IS TO IMPLEMENT THE CODE FOR THE FOLLOWING METHODS 
	(refer to the api docs for the Bike web service)
	Once you get all that working, you can start working on the BikeDetails module (see rpgbacon/Bike-details.js)
	This module will display an HTML form that will allow users to add new Bikes, and edit existing Bikes.
	*/

	function getAllBikes(callback){

		rpgbacon.ajax.send({
			callback:callback,
			url:urlWebService,
			method:"GET"
		})

		
	}

	function getBikeById(id, callback){
		// TODO: use your rpgbacon.ajax module to send the proper request to the Bike web service
		// to get a Bike by it's id

		// Note: you can comment out this line of code...
		rpgbacon.ajax.send({
			callback:callback,
			url:urlWebService + id,
			method:"GET"
		})
	}

	function postBike(Bike, callback){
		// TODO: use your rpgbacon.ajax module to send the proper request to the Bike web service
		// in order to add a Bike

		// Note: you can comment out this line...
		rpgbacon.ajax.send({
			callback:callback,
			url:urlWebService,
			requestBody: JSON.stringify(Bike),
			method:"POST"
		})
		// callback("TODO: INSERT NEW Bike: " + JSON.stringify(Bike));
	}


	function putBike(Bike, callback){
		// TODO: use your rpgbacon.ajax module to send the proper request to the Bike web service
		// in order to add a Bike

		// Note: you can comment out this line...
		rpgbacon.ajax.send({
			callback:callback,
			url:urlWebService + Bike.id,
			requestBody: JSON.stringify(Bike),
			method:"POST"
		})

		// callback("TODO: UPDATE A Bike: " + JSON.stringify(Bike));
	}

	function deleteBike(id, Bike, callback){
		// TODO: use your rpgbacon.ajax module to send the proper request to the Bike web service
		// in order to add a Bike

		// Note: you can comment out this line...
		rpgbacon.ajax.send({
			callback:callback,
			url:urlWebService + id,
			requestBody: JSON.stringify(Bike),
			method:"POST"
		})

		// callback("TODO: UPDATE A Bike: " + JSON.stringify(Bike));
	}

	return{
		getAllBikes: getAllBikes,
		getBikeById: getBikeById,
		postBike: postBike,
		putBike: putBike,
		deleteBike: deleteBike
	}
}