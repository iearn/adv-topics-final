var rpgbacon = rpgbacon || {};

/**
*  	@module rpgbacon
*
*	@class BikeDetails
*
*	@main The rpgbacon module lets users create, update, and delete bikes.
*/
rpgbacon.BikeDetails = {
	
	init: function(options){

		/**
        * The container that will hold the BikeDetails module.
        *
        * @poperty target
        * @type DOM Element
        *
        */
		// Instance Vars
		var target = options.target;
		var bike = options.bike;
		var saveCallBack = options.saveCallBack;
		
		if (!bike) {
			bike = {id : 0, title : "", author : ""};
		}

		createUI();


		// Methods

		function createUI() {

			hidden = document.createElement("input");
			hidden.setAttribute("type", "hidden");

			txtCompany = document.createElement("h3");
			txtCompany.innerHTML = "Company";

			companyInput = document.createElement("input");
			companyInput.setAttribute("type", "text");

			txtYear = document.createElement("h3");
			txtYear.innerHTML = "Year";

			yearInput = document.createElement("input");
			yearInput.setAttribute("type", "text");

			txtColor = document.createElement("h3");
			txtColor.innerHTML = "Color";

			colorInput = document.createElement("input");
			colorInput.setAttribute("type", "text");



			btnSave = document.createElement("button");
			btnSave.setAttribute("type", "button");
			btnSave.addEventListener('click', function(){

				if (validate()) {
					getDataFromUI();
					
					if(saveCallBack){
						console.log(bike);
						saveCallBack(bike);
					}
				}

			});

			btnSave.innerHTML = "save";

			btnReset = document.createElement("button");
			btnReset.setAttribute("type", "button");
			btnReset.addEventListener('click', function(){
				clearUI();
				
				bike = {id : 0, title : "", author : ""};


			});

			btnReset.innerHTML = "reset";

			target.appendChild(hidden);

			target.appendChild(txtCompany);
			target.appendChild(companyInput);
			target.appendChild(txtYear);
			target.appendChild(yearInput);
			target.appendChild(txtColor);
			target.appendChild(colorInput);

			target.appendChild(btnSave);
			target.appendChild(btnReset);

		}

	/**
     * Updates a bike. The callback function will have the a JSON bike object.
     *
     * @method setBike
     * @param {Object} bike         A bike object will display.
     */
		function setBike(newBike){
			bike = newBike;
			hidden.value = bike.id;
			companyInput.value = bike.company;
			yearInput.value = bike.year;
			colorInput.value = bike.color;
		}

		function clearUI(){
			hidden.value = 0;
			companyInput.value = "";
			yearInput.value = "";	
			colorInput.value = "";	
		}

		function getDataFromUI(){
			bike.id = hidden.value;
			bike.company = companyInput.value;
			bike.year = yearInput.value;
			bike.color = colorInput.value;
		}
		

		function validate(){

			var isValid = false

			if (companyInput.value == "") {
				alert("Company of the bike is empty.");
				return isValid;
			}

			if (yearInput.value == "") {
				alert("Year name is empty");
				return isValid;
			}

			if (colorInput.value == "") {
				alert("Color name is empty");
				return isValid;
			}

			isValid = true;

			return true;


		}

		// Return the public API
		return{
			setBike:setBike
		};
	}

};