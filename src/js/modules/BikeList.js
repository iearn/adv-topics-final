var rpgbacon = rpgbacon || {};

/**
*  	@module rpgbacon
*
*	@class BikeList
*
*/
rpgbacon.BikeList = {
	
	init: function(options){

		/**
        * The container that will hold the BikeList module.
        *
        * @poperty target
        * @type DOM Element
        *
        * @poperty data
        * @type An array with Bike object inside
        */
		
		// Instance Vars
		var target = options.target;
		var data = options.data;
		var selectedCallBack = options.selectedCallBack;
		var deleteBikeCallBack = options.deleteBikeCallBack;

		target.appendChild(createUI());

		// Methods
		function createUI(){

			var ul = document.createElement("ul");

			for (var i = data.length - 1; i >= 0; i--) {
				// console.log(data[i]);

				var li = document.createElement("li");
  	 	 	
				li.innerHTML = data[i].company;
				li.innerHTML += " year: ";
				li.innerHTML += data[i].year;
				li.innerHTML += " color: ";
				li.innerHTML += data[i].color;
				// li.innerHTML += ", Runs: ";
				// li.innerHTML += data[i].active;

				var btnEdit = document.createElement("button");
				btnEdit.setAttribute("data-Bike-id", data[i].id);
				btnEdit.addEventListener("click", function(evt){
					if(selectedCallBack){
						var BikeId = evt.target.getAttribute("data-Bike-id");
						var Bike = getBikeById(BikeId);
						selectedCallBack(Bike);
					}
				});
				btnEdit.innerHTML= "EDIT";

				var btnDelete = document.createElement("button");
				btnDelete.setAttribute("data-Bike-id", data[i].id);
				btnDelete.addEventListener("click", function(evt){
					if(deleteBikeCallBack){
						var BikeId = evt.target.getAttribute("data-Bike-id");
						var Bike = getBikeById(BikeId);
						deleteBikeCallBack(Bike);
					}
				});
				btnDelete.innerHTML= "DELETE";

				li.appendChild(btnEdit);
				li.appendChild(btnDelete);
				ul.appendChild(li);

			}
			return ul;

		}

		function getBikeById(id){
			for (var i = 0; i < data.length; i++) {

				if (id == data[i].id) {
					return data[i];
				}
			}
		}
		
		/**
	     * Removes a bike from the booklist
	     *
	     * @method removeBike
	     * @param {Int}id		Removes the bike from the booklist with that id.
	     */
		function removeBike(id){
			for (var i = 0; i < data.length; i++) {
				if(id == data[i].id){
					console.log(id + " | " + data[i].id);
					data.splice(i, 1);
				}
			}
			console.log(data);
			refresh();
		}
		
		/**
	     * Adds a bike to the bike list
	     *
	     * @method addBike
	     * @param {obj}Bike		Displays a the bike passed into method into the BikeList module
	     */
		function addBike(Bike){
			data.push(Bike);
			refresh();
		}

		/**
	     * Clears the bike model object and then rebuilds it.
	     *
	     * @method refresh
	     */
		function refresh(){
			target.innerHTML = "";

			target.appendChild(createUI());
		}

		// Return the public API
		return{
			refresh: refresh,
			addBike: addBike,
			removeBike: removeBike
		};
	}

};