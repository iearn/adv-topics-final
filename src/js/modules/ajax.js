// Short hand
var rpgbacon = rpgbacon || {};

// Long hand
// if (acme) {
// 
// }else{
	// acme = {}
// }

rpgbacon.ajax = {
	send: function(options){

		var http = new XMLHttpRequest();
		var callback = options.callback;
		var url = options.url;
		var method = options.method;
		var headers = options.headers;
		var requestBody = options.requestBody;

		http.open(method, url);

		if (headers) {
			for(var p in headers){
			http.setRequestHeader(p, headers[p]);
			}
		}

	    http.onreadystatechange = function() {
	    // http.addEventListner('readystatechange', function() { //is the same above
	        if(http.readyState == 4 && http.status == 200) {
	            callback(http.responseText);
	        }else if(http.readyState == 4){
	        	alert("Error" + http.responseText);
	        }
	    };

	    http.send(requestBody);	

	},
	error: function(errMsg){
		alert(errMsg);
	}
};