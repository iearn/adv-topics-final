<?php

class BikeDataAccess{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}


	/**
	* Get all bike
	* @param $order_by 		Optional - the sort order (company or year)
	* @return 2d array 		Returns an array of bikes (each bike is an assoc array)
	*/
	function get_all_bikes($order_by = null){
		$qStr = "SELECT	id, company, year, color, active 
			FROM bikes
			WHERE active = 'yes'";

		if($order_by == "company" || $order_by == "year"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$all_bikes = array();

		while($row = mysqli_fetch_assoc($result)){

			$bike = array();
			$bike['id'] = htmlentities($row['id']);
			$bike['company'] = htmlentities($row['company']);
			$bike['year'] = htmlentities($row['year']);
			$bike['color'] = htmlentities($row['color']);
			$bike['active'] = htmlentities($row['active']);

			$all_bikes[] = $bike;
		}

		return $all_bikes;
	}


	/**
	* Get a bike by its ID
	* @param $id 			The ID of the bike to get
	* @return array 		An assoc array that has keys for each property of the bike
	*/
	function get_bike_by_id($id){

		$qStr = "SELECT	id, company, year, color, active FROM bikes WHERE id = " . mysqli_real_escape_string($this->link, $id);

				
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$bike = array();
			$bike['id'] = htmlentities($row['id']);
			$bike['company'] = htmlentities($row['company']);
			$bike['year'] = htmlentities($row['year']);
			$bike['color'] = htmlentities($row['color']);
			$bike['active'] = htmlentities($row['active']);
			return $bike;

		}else{
			return null;
		}
			
	}


	function insert_bike($bike){

		// prevent SQL injection
		$bike['company'] = mysqli_real_escape_string($this->link, $bike['company']);
		$bike['year'] = mysqli_real_escape_string($this->link, $bike['year']);
		$bike['color'] = mysqli_real_escape_string($this->link, $bike['color']);
		// $bike['active'] = mysqli_real_escape_string($this->link, $bike['active']);
		

		$qStr = "INSERT INTO bikes (
					company,
					year,
					color,
					active
				) VALUES (
					'{$bike['company']}',
					'{$bike['year']}',
					'{$bike['color']}',
					'yes'
				)";
		
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the bike id that was assigned by the data base
			$bike['id'] = mysqli_insert_id($this->link);
			// then return the bike
			return $bike;
		}else{
			$this->handle_error("unable to insert bike");
		}

		return false;
	}



	function update_bike($bike, $act=true){

		// prevent SQL injection
		$bike['id'] = mysqli_real_escape_string($this->link, $bike['id']);
		$bike['company'] = mysqli_real_escape_string($this->link, $bike['company']);
		$bike['year'] = mysqli_real_escape_string($this->link, $bike['year']);
		$bike['color'] = mysqli_real_escape_string($this->link, $bike['color']);

		if ($act) {
		
		$bike['active'] = mysqli_real_escape_string($this->link, $bike['active']);


		$qStr = "UPDATE bikes SET 
		company='{$bike['company']}', 
		year='{$bike['year']}', 
		color='{$bike['color']}' 
		WHERE id = " . $bike['id'];

		}else{
		
		$qStr = "UPDATE bikes SET 
		company='{$bike['company']}', 
		year='{$bike['year']}', 
		color='{$bike['color']}', 
		active='no' 

		WHERE id = " . $bike['id'];
		}

		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $bike;
		}else{
			$this->handle_error("unable to update bike");
		}

		return false;
	}
	


}