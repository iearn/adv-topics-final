<!DOCTYPE html>
<html>
<head>
	<title>Bikes Service API Docs</title>
</head>
<body>

<h1>Official API Docs for the Bikes Web Service</h1>
  <table border="1">
    <tr>
      <th>API CALL (ROUTE)</th>
      <th>METHOD</th>
      <th>ACTION</th>
    </tr>
    <tr>
      <td>bikes/</td>
      <td>GET</td>
      <td>Get's all bikes</td>
    </tr>
    <tr>
      <td>bikes/</td>
      <td>POST</td>
      <td>
        Inserts a new bike into the data base <br>
        Expects the Content-Type to be application/json
      </td>
    </tr>
    <tr>
      <td>bikes/?order_by=x</td>
      <td>GET</td>
      <td>
        Get's all bikes, ordered by a property of a bike <br>
        Note: x could be 'company name' or 'year'
      </td>
    </tr>
    <tr>
      <td>bikes/x</td>
      <td>GET</td>
      <td>
        Get's a bike by it's id property <br>
        ex: <b>bikes/1</b> would fetch the bike with an id of 1
      </td>
    </tr>
    <tr>
      <td>bikes/x</td>
      <td>PUT</td>
      <td>
        Edits the bike with id of x <br>
        Note: then Content-Type should be application/json
      </td>
    </tr>
  </table>
  ***By default, responses will return data in JSON format <br>
  ***BUT IF THE 'Accept' request header is set to 'application/xml' THEN WE RETURN THE DATA IN XML FORMAT

</body>
</html>