<?php
  header("Access-Control-Allow-Origin: *");

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  if($_SERVER['SERVER_NAME'] == "localhost"){
      define("DB_HOST", "localhost");
      define("DB_USER", "root");
      define("DB_PASSWORD", "");
      define("DB_NAME", "sample_db");
  }else{
      define("DB_HOST", "localhost");
      define("DB_USER", "rpgbacon_some");
      define("DB_PASSWORD", "12345678abc");
      define("DB_NAME", "rpgbacon_bike-service");
  }

// Pre-requisites:
// 	1. Url Re-writing (mod rewrite)
//	2. AJAX - We should have completed the Abstracting AJAX project
//	3. Regular expressions


///////////////////////////////////
// Set up the data access object
///////////////////////////////////
// echo DB_HOST . " " . DB_USER . " " . DB_PASSWORD . " " . DB_NAME; 

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
require("BikeDataAccess.inc.php");
$da = new BikeDataAccess($link);


///////////////////////////////////
// Handle the Request
///////////////////////////////////

// Gather all the information about the request
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : ""; // This is the path part of the URL being requested (see the .htaccess file)
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input'); // note that GET requests do not have request body
$request_headers = getallheaders();


// This IF statement will check to see if the requested URL (and method) is supported by this web service
// If so, then we need to formulate the proper response, if not then we return a 404 status code
if($method == "GET" && empty($url_path)){

 	//Show the home page for this web service
  require("api-docs.php");
  die();

}else if($method == "GET" && ($url_path == "bikes/" || $url_path == "bikes")){

  // die($method . "<br>" . $url_path);
  get_all_bikes();

}else if($method == "POST" && ($url_path == "bikes/" || $url_path == "bikes")){

    // die($method . "<br>" . $url_path);
  insert_bike();
  
}else if($method == "GET" && preg_match('/^bikes\/([0-9]*\/?)$/', $url_path, $matches)){
   
  // die($method . "<br>" . $url_path);
  // Get the id of the bike from the regular expression
  $bike_id = $matches[1];
  get_bike_by_id($bike_id);

}else if($method == "POST" && preg_match('/^bikes\/([0-9]*\/?)$/', $url_path, $matches)){

  // die($method . "<br>" . $url_path);
  // Get the id of the bike from the regular expression
  $bike_id = $matches[1]; // we can get the bike id from the request body
  update_bike();  
  
}else if($method == "POST" && preg_match('/^bikes\/([0-9]*\/?)$/', $url_path, $matches)){
  // die($method . "<br>" . $url_path);

  $bike_id = $matches[1];
  delete_bike();
}else{

  header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_URI']}");

}


/////////////////////////////////
// FUNCTIONS
/////////////////////////////////

function get_all_bikes(){

  global $da, $url_path, $method, $request_body, $request_headers;
  
  // check to see if we need to sort the bikes by company or year
  $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : null;
  
  // get the bikes from the database
  $bikes = $da->get_all_bikes($order_by);
  // var_dump($bikes);
  // die();
  if(!$bikes){
    header('HTTP/1.1 500 server error (fetching bikes from the database)', true, 500);
    die();
  }
    
  // We'll default to returning bikes in JSON format unless the client
  // requests XML (the Accept header in the request would be set to application/xml)
  $return_format = $request_headers['Accept'];
  
  if($return_format == "application/xml"){
    //header("Content-Type","application/xml");
    die("TODO: convert bike data to XML");
  }else{
    // header("Content-Type","application/json");
    $json = json_encode($bikes);
    echo($json);
    die();
  }

}


function insert_bike(){

  global $da, $url_path, $method, $request_body, $request_headers;

  // The data for the bike being inserted should be in the request body.
  // The data should be sent in the JSON format which means that the Content-Type header in the request SHOULD be set to application/json
  // But if it's not set properly, we'll just assume the data is coming in as JSON
  // In the future, we might also accept the data in the request body to be XML, which would mean that the Content-Type header is set to application/xml
  
  $new_bike = null;
  
  if($request_headers['Content-Type'] == "application/xml"){
    die("TODO: convert XML bike data into an associative array");    
  }else{
    // convert the json to an associative array
    // note: json_decode() will return false if it can't convert the request body (maybe because it's not valid json)
    $new_bike = json_decode($request_body, true);
  }

  // TODO: validate the $new_bike assoc array (make sure it has id, year and company keys)

  if($new_bike){
    
    if($new_bike = $da->insert_bike($new_bike)){
      // note that the bike returned by insert_bike() will have the id set (by the database auto increment)
      // TODO: if the Accept header in the request is set to application/xml, then the client want the data returned in XML, we could deal with that later  
      // For now, we'll just return the data in JSON format
      // header("Content-Type","application/json");
      die(json_encode($new_bike));
    }else{
      header('HTTP/1.1 500 server error (fetching bikes from the database)', true, 500);
      die();  
    }
  
  }else{
    header('HTTP/1.1 400 - the data sent in the request is not valid', true, 400);
    die();
  }

}


function get_bike_by_id($id){

  global $da, $url_path, $method, $request_body, $request_headers;

  $bike = $da->get_bike_by_id($id);

  // TODO: we may want to check the Accept header in the request, and if it's set to application/xml, we might return the data as XML instead of JSON
  // But for now, we'll just return the bike data as JSON
  
  if($bike){
    // header("Content-Type","application/json");
    die(json_encode($bike));
  }else{
    header('HTTP/1.1 400 - the bike id in the requested url is not in the database', true, 400);
    die();
  }

}


function update_bike(){

  global $da, $url_path, $method, $request_body, $request_headers;

  if($bike = json_decode($request_body, true)){

    $bike = $da->update_bike($bike);
  
    if($bike){
      // header("Content-Type","application/json");
      die(json_encode($bike));
    }else{
      header('HTTP/1.1 500 - Unable to update bike in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid bike data in the request body', true, 400);
    die();
  }

}

function delete_bike(){

    global $da, $url_path, $method, $request_body, $request_headers;

    if($bike = json_decode($request_body, true)){

    $bike = $da->update_bike($bike, false);

    if($bike){
      // header("Content-Type","application/json");
      die(json_encode($bike));
    }else{
      header('HTTP/1.1 500 - Unable to update bike in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid bike data in the request body', true, 400);
    die();
  }
}

?>